# coding=utf8
# Example: python main.py http://forum.igromania.ru
__author__ = 'flopik'

import re
import sys
import urllib2

searchLogin = '<a href="member.php\?s=.+">.+<\/a>'
trashStart = '<a href="member.php\?s=.+">'
header = {}

def killTrash(content):
    content = re.sub(re.compile(trashStart), '', content)
    content = re.sub(re.compile('<.+>'), '', content)

    return content

def math(page):
    req = urllib2.Request(sys.argv[1]+'/memberlist.php?&order=asc&sort=username&page=%i' % page, None, header)

    content = urllib2.urlopen(req).read()

    return re.findall(re.compile(searchLogin), content)

iter = 1

opennerLog = open('nick.log', 'w')

while True:
    nickList = math(iter)

    for line in nickList:
        if(len(line) > 3):
            nick = killTrash(line)
            opennerLog.write(nick+'\n')

    print "Page %i" % iter
    iter += 1

opennerLog.close()

print "END"